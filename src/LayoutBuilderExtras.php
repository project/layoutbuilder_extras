<?php

namespace Drupal\layoutbuilder_extras;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Form\FormStateInterface;
use Drupal\layout_builder\Controller\LayoutRebuildTrait;
use Drupal\layout_builder\SectionStorageInterface;
use Drupal\layout_builder\Form\ConfigureSectionForm;

/**
 * Callbacks for the ajax live reloading.
 */
class LayoutBuilderExtras {

  use LayoutRebuildTrait;

  /**
   * Saves the form. So that the settings that you change are saved.
   *
   * Because on move HTML gets updates and shit gets lost.
   *
   * @param array $form
   *   Form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   An ajax response to maybe change the content.
   */
  public function ajaxSave(array &$form, FormStateInterface $form_state) {
    /* @var \Drupal\layoutbuilder_extras\Form\LayoutBuilderExtrasConfigureSectionForm $configureSectionForm */
    $configureSectionForm = $form_state->getBuildInfo()['callback_object'];

    if ($configureSectionForm->isUpdate()) {
      // Needed for change in TPL.
      /* @var SectionStorageInterface $section_storage */
      $section_storage = $form_state->getBuildInfo()['args'][0];

      $this->validateAndSubmitConfigureSectionForm($form_state, $form);

      return $this->rebuildLayout($section_storage);
    }
    else {
      // Return empty response.
      return new AjaxResponse();
    }

  }

  /**
   * Validate and submitted configure section form.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   * @param array $form
   *   The form.
   *
   * @return \Drupal\layout_builder\Form\ConfigureSectionForm
   *   A configure section form that is validated an submitted.
   */
  private function validateAndSubmitConfigureSectionForm(FormStateInterface $form_state, array &$form): ConfigureSectionForm {
    /* @var \Drupal\layout_builder\Form\ConfigureSectionForm $configureSectionForm */
    $configureSectionForm = $form_state->getBuildInfo()['callback_object'];
    // Needed for visual change.
    $configureSectionForm->validateForm($form, $form_state);
    $configureSectionForm->submitForm($form, $form_state);
    return $configureSectionForm;
  }

}
