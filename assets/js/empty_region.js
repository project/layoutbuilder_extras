(function (Drupal) {

  Drupal.behaviors.layoutBuilderExtrasDisableLBPreview = {
    attach: function attach(context) {

      const classEmptyRegion = 'layout-builder-empty-region';

      const elementsEmptyRegion = document.querySelectorAll('.' + classEmptyRegion);

      if (elementsEmptyRegion) {
        elementsEmptyRegion.forEach(function(elementEmptyRegion) {
          elementEmptyRegion.addEventListener('click', function(e) {
            e.preventDefault();
            e.stopPropagation();

            e.target.parentElement.parentElement.querySelector('.layout-builder__link--add').click();
          })
        });
      }

    }
  };


})(Drupal);
